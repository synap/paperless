FROM ubuntu:latest
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends curl python3-sane python3-swiftclient ca-certificates \
    && curl https://download.brother.com/welcome/dlf100976/libsane-dsseries_1.0.5-1_amd64.deb -o /tmp/libsane-dsseries_1.0.5-1_amd64.deb \
    && dpkg -i /tmp/libsane-dsseries_1.0.5-1_amd64.deb \
    && rm /tmp/libsane-dsseries_1.0.5-1_amd64.deb
COPY ./scan-documents /usr/local/bin/scan-documents
CMD scan-documents
