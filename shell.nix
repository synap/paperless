with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/cd63096d6d887d689543a0b97743d28995bc9bc3.tar.gz") {};
( let
    sane = python38.pkgs.buildPythonPackage rec {
      pname = "python-sane";
      version = "2.8.2";

      src = python38.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "0sri01h9sld6w7vgfhwp29n5w19g6idz01ba2giwnkd99k1y2iqg";
      };

      buildInputs = [ pkgs.sane-backends ];
    };

  in python38.withPackages (ps: [sane ps.pillow])
).env
