build:
	docker build -t scanner .

scan:
	docker run -it -u `id -u`:`id -g` -v ${PWD}/images:/images --device=/dev/sg1 scanner scan-documents

shell:
	docker run -it -u `id -u`:`id -g` -v ${PWD}/images:/images --device=/dev/sg1 scanner bash
